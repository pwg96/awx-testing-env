## awx testing environment 
awx testing environment based on vagrant and virtualbox

### Requirments

* Vagrant 2.x: can be downloaded from here https://www.vagrantup.com/downloads.html
* Virtualbox

Installation of those components depends on your operating system. When in doubt, look for official documentation and follow it.

### Usage

Check out `inventory` file if you want to set proper passwords and stuff like that.
After doing so simply run `vagrant up` and you're ready to go!

You should get:
* awx machine with internal IP `10.0.0.1`, and port `8888` exposed on your host for webui access (http://localhost:8888)
* three testing nodes with internal IPs `10.0.0.2 - 10.0.0.4` with example deploy key provisioned.

### Version
awx 2.1.2: https://github.com/ansible/awx/tree/2.1.2

### awx docs
Installation guide: https://github.com/ansible/awx/blob/devel/INSTALL.md
Official documentation: https://docs.ansible.com/ansible-tower/index.html

### Trivia
awx is an open source project from which Red Hat's Ansible Tower is derived.
